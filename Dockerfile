FROM ubuntu:20.04 as workspace

LABEL tag=workspace

### install and configure tzdata
RUN apt update && \
    ln -fs /usr/share/zoneinfo/Europe/Moscow /etc/localtime && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata && \
    dpkg-reconfigure --frontend noninteractive tzdata

### update package and install mariadb, php7.4, apache2, yii2, running mariadb
RUN apt -y install mariadb-server apache2 php git zip phpunit php-curl php-imagick && \
    a2enmod rewrite && \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === 'e21205b207c3ff031906575712edab6f13eb0b361f2085f1f1237b7126d785e826a450292b6cfd1d64d92e6563bbde02') \
    { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php --install-dir=bin --filename=composer && \
    composer create-project --prefer-dist yiisoft/yii2-app-basic basic && \
    mkdir /basic/log && \
    echo "ServerName localhost" >> /etc/apache2/apache2.conf

COPY 000-default.conf /etc/apache2/sites-enabled/000-default.conf
WORKDIR basic

COPY script.sh ./script.sh
COPY test.sh ./test.sh

CMD sh ./script.sh